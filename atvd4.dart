import 'Dart:math';
import 'dart:io';
//4) Ler uma string e verificar se ela é um palindromo

void main() {
  stdout.writeln('Digite uma palavra para verificar se é palíndromo:\n');

  var entry = stdin.readLineSync();
  var isPalindrome = true;

  for (var i = 0; i < entry!.length / 2; i++) {
    entry[i] == entry[entry.length - i - 1] ? true : isPalindrome = false;
  }

  isPalindrome
      ? stdout.writeln('\né palindromo')
      : stdout.writeln('\nnão é palindromo');
}

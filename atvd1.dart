import 'dart:io';

void main() {
  List<int> lista = [];
  var op = true;
  stdout.write('Digite as notas');
  while (op) {
    var nota = stdin.readLineSync();
    lista.add(int.parse(nota!));
    stdout.writeln('Digite 1 - Para adicionar outra');
    stdout.writeln('Digite 2 - Para parar');
    var resp = stdin.readLineSync();
    resp == '1' ? op = true : op = false;
  }

  stdout.writeln('Notas: ');
  print(lista);
  stdout.writeln('Total:');
  print(lista.reduce((value, l) => value + l));
}
